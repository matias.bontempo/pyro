/* eslint-disable max-classes-per-file */

const $ = (s) => document.querySelector(s);

const EYE_DELAY = 1000;
const BLOW_DELAY = 3000;
const BLOW_LENGTH = 1000;
const LONG_PRESS = 500;

const MOUSE_THRESHOLD = 50;
const FRAME_DURATION = 120;

const state = {
  blow_out_mode: false,
  light_candles: false,
  short: false,
  long: false,
  lastMouseDown: 0,
  lastMouseY: 0,
};


class AnimatedElement {
  constructor($el, w, h, vertical) {
    this.$el = $el;
    this.w = w;
    this.h = h;

    this.vertical = vertical;

    this.frame = 0;

    this.show = true;

    this.totalFrames = w * h;
    this.frameDuration = FRAME_DURATION;
    this.oneShot = false;

    this.animation = null;

    this.lastFrameUpdate = Date.now();
    this.definedFrameDuration = this.frameDuration;

    this.tileWidth = $el.offsetWidth;
    this.tileHeight = $el.offsetHeight;
  }

  setAnimation(a, f) {
    if (!a) {
      this.totalFrames = this.w * this.h;
      this.animation = null;
      this.definedFrameDuration = this.frameDuration;
      return;
    }

    this.animation = a;
    this.totalFrames = a.length;
    this.definedFrameDuration = f || this.frameDuration;

    this.frame = 0;
  }

  setFrame(n) {
    const frame = n % this.totalFrames;
    this.frame = frame;
    this.draw();
  }

  getOffset() {
    let x = 0;
    let y = 0;

    const realFrame = this.animation ? this.animation[this.frame] : this.frame;

    if (this.vertical) {
      x = Math.floor(realFrame / this.h);
      y = realFrame % this.h;
    } else {
      x = realFrame % this.w;
      y = Math.floor(realFrame / this.w);
    }

    return { x: x * this.tileWidth, y: y * this.tileHeight };
  }

  update() {
    if (!this.show) {
      this.$el.style.backgroundSize = 0;
      return;
    }

    this.$el.style.backgroundSize = 'auto';

    const now = Date.now();

    if ((this.lastFrameUpdate + this.definedFrameDuration) < now) {
      this.lastFrameUpdate = now;
      this.frame += 1;
      if (this.oneShot && this.frame > this.totalFrames) {
        this.show = false;
      }
      if (!this.oneShot) this.frame %= this.totalFrames;
      this.draw();
    }
  }

  draw() {
    const offset = this.getOffset();
    this.$el.style.backgroundPosition = `-${offset.x}px -${offset.y}px`;
  }
}

class Eyes extends AnimatedElement {
  constructor() {
    if (Eyes.instance) return Eyes.instance;

    super($('#eyes'), 2, 10, true);

    this.frameDuration = 30;

    this.glowing = false;

    this.animations = {
      glowDown: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
      glowUp: [19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0],
      waitDown: [19],
      waitUp: [0, 1],
    };

    this.setAnimation(this.animations.waitDown);

    this.lastUpdate = Date.now();

    Eyes.instance = this;
  }

  update() {
    super.update();

    if (this.animation === this.animations.waitUp || this.animation === this.animations.waitDown) {
      const now = Date.now();
      if ((this.lastUpdate + EYE_DELAY) < now) {
        this.lastUpdate = now;
        this.glowing = !this.glowing;

        if (this.glowing) this.setAnimation(this.animations.glowUp);
        else this.setAnimation(this.animations.glowDown);
      }
    }

    if (this.animation === this.animations.glowDown && this.frame === this.totalFrames - 1) {
      this.lastUpdate = Date.now();
      this.setAnimation(this.animations.waitDown, 120);
    }

    if (this.animation === this.animations.glowUp && this.frame === this.totalFrames - 1) {
      this.lastUpdate = Date.now();
      this.setAnimation(this.animations.waitUp, 120);
    }
  }
}

class Smoke extends AnimatedElement {
  constructor($el) {
    super($el, 8, 1);
    this.show = false;
    this.frameDuration = 120;
    this.oneShot = true;
  }

  start() {
    this.show = true;
    this.frame = 0;
  }
}

class Candle extends AnimatedElement {
  constructor(i) {
    super($(`#candle-${i}`), 4, 2);
    this.id = i;

    this.show = state.light_candles;

    const smoke = document.createElement('div');
    smoke.classList.add('smoke');
    this.$el.appendChild(smoke);

    this.smoke = new Smoke(smoke);

    this.animations = {
      idle: [0, 1, 2, 3],
      blowing: [4, 5, 6, 7],
      afterBlow: [0, 1, 2, 3, 4, 5, 6, 7],
      flicker: [0, 1, 2, 3, 6],
    };

    this.blowing = false;
    this.blowTimer = 0;

    this.shouldBlowOut = false;

    this.afterBlow = false;
    this.afterBlowTimer = 0;

    this.setAnimation(this.animations.idle);
  }

  blow(blowOut) {
    if (!this.show) return;
    this.blowing = true;
    this.blowTimer = Date.now() + BLOW_LENGTH;
    this.shouldBlowOut = blowOut;
    this.setAnimation(this.animations.blowing);
  }

  update() {
    super.update();
    this.smoke.update();

    if (this.animation) {
      const randomFrame = Math.floor(Math.random() * this.animation.length);
      this.frame = this.animation[randomFrame];
    }

    if (this.show && this.blowing) {
      if (this.blowTimer < Date.now()) {
        this.blowing = false;

        if (this.shouldBlowOut) {
          this.show = false;
          this.smoke.start();
        } else {
          this.afterBlow = true;
          this.afterBlowTimer = Date.now() + BLOW_LENGTH;
          this.setAnimation(this.animations.afterBlow);
        }
      }
    }

    if (this.show && this.afterBlow) {
      if (this.afterBlowTimer < Date.now()) {
        this.blowing = false;
        this.afterBlow = false;
        this.setAnimation(this.animations.idle);
      }
    }

    if (!this.shouldBlowOut && this.show !== state.light_candles) {
      if (state.blow_out_mode && this.id === 0 && this.animation !== this.animations.flicker) {
        this.setAnimation(this.animations.flicker);
      }

      if (Math.random() > 0.985) {
        this.show = state.light_candles;
        if (this.show) {
          this.setAnimation(this.animations.idle);
        }
        if (!this.show) this.smoke.start();
      }
    }

    if (this.shouldBlowOut && !state.light_candles) {
      this.shouldBlowOut = false;
    }
  }
}

class Walls {
  constructor(candles) {
    if (Walls.instance) return Walls.instance;

    this.$back = $('#background-candles');
    this.$left = $('#background-middle-left-lit');
    this.$right = $('#background-middle-right-lit');

    this.candles = candles;

    Walls.instance = this;
  }

  update() {
    this.$left.style.opacity = this.candles[0].show ? 1 : 0;
    this.$right.style.opacity = this.candles[3].show ? 1 : 0;

    const total = this.candles.reduce((a, c) => a + c.show, 0);

    if (total === 0) this.$back.style.backgroundPosition = '0 0';
    else if (total === 1) this.$back.style.backgroundPosition = `-${this.$back.offsetWidth}px 0`;
    else this.$back.style.backgroundPosition = `-${this.$back.offsetWidth * 2}px 0`;
  }
}


// Initialization

const eyes = new Eyes();
const candles = [];
for (let i = 0; i < 4; i += 1) candles[i] = new Candle(i);
const walls = new Walls(candles);


// Prevent context menu on long press
window.oncontextmenu = (event) => {
  event.preventDefault();
  event.stopPropagation();
  return false;
};


// Scale container to full screen

const scaleContainer = () => {
  const $container = $('#container');
  const cAspectRatio = $container.offsetWidth / $container.offsetHeight;
  const windowAspectRatio = window.innerWidth / window.innerHeight;

  let toScale = 1;


  if (cAspectRatio > windowAspectRatio) toScale = window.innerWidth / $container.offsetWidth;
  else toScale = window.innerHeight / $container.offsetHeight;

  $container.style.transform = `scale(${toScale})`;
};


// Main logic

const handleMouseDown = (e) => {
  e.preventDefault();
  state.lastPress = Date.now();
  state.lastMouseY = e.pageY || e.touches[0].clientY;
};

const handleMouseUp = (e) => {
  e.preventDefault();
  const posY = e.pageY || e.changedTouches[0].clientY;

  const longPress = (Date.now() - state.lastPress) > LONG_PRESS;
  const secretMove = (posY - state.lastMouseY) > MOUSE_THRESHOLD;

  if (state.light_candles && longPress) {
    if (state.blow_out_mode) {
      state.blow_out_mode = false;
    } else if (secretMove) {
      state.blow_out_mode = true;
    }
  }

  state.light_candles = !state.light_candles;

  // eslint-disable-next-line no-bitwise
  const candleToBlow = (eyes.glowing << 1) | eyes.glowing ^ longPress;

  if (state.blow_out_mode && state.light_candles) {
    setTimeout(() => {
      candles.forEach((c, i) => c.blow(i === candleToBlow));
    }, BLOW_DELAY);
  }
};

document.addEventListener('mousedown', handleMouseDown);
document.addEventListener('mouseup', handleMouseUp);

document.addEventListener('touchstart', handleMouseDown, false);
document.addEventListener('touchend', handleMouseUp, false);


// Main loop

const loop = () => {
  eyes.update();
  candles.forEach((c) => c.update());
  walls.update();
  window.requestAnimationFrame(loop);
};


const init = () => {
  scaleContainer();
  loop();
};

document.addEventListener('DOMContentLoaded', init, false);
